const {uuid} = require('uuidv4');
const {on} = require('events');

const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http,
    {
      cors: {
        origin: true,
        // methods:  "GET,HEAD,PUT,PATCH,POST,DELETE",
      }
    }
);
const port = process.env.PORT || 3000;

const CONNECT = "connect"

const LOGIN = "LOGIN"
const LOGIN_SUCCESS = "LOGIN_SUCCESS"
const LOGIN_FAILED = "LOGIN_FAILED"

const LOGOUT = "LOGOUT"

const GET_REMINDERS = "GET_REMINDERS"
const RECEIVE_REMINDERS = "RECEIVE_REMINDERS"

const ADD_REMINDER = "ADD_REMINDER"
const ADDED_REMINDER = "ADDED_REMINDER"

const UPDATE_REMINDER = "UPDATE_REMINDER"
const UPDATED_REMINDER = "UPDATED_REMINDER"

const DELETE_REMINDER = "DELETE_REMINDER"
const DELETED_REMINDER = "DELETED_REMINDER"

const CHANGE_USERNAME = "CHANGE_USERNAME"
const GET_UPDATE_USER = "GET_UPDATE_USER"
const CHANGE_USERNAME_SUCCESS = "CHANGE_USERNAME_SUCCESS"
const CHANGE_USERNAME_FAILED = "CHANGE_USERNAME_FAILED"

let reminders = [];
const users = [];


io.on(CONNECT, socket => {
  console.log(`new  connection from ${socket.handshake.address}`);

  socket.on(LOGIN, (user, request) => login(user, request, socket));
  socket.on(ADD_REMINDER, (reminder) => addReminder(reminder));
  socket.on(GET_REMINDERS, (user) => getReminders(user, socket));
  socket.on(UPDATE_REMINDER, (reminder) => updateReminder(reminder));
  socket.on(DELETE_REMINDER, (reminder) => deleteReminder(reminder, socket));
  socket.on(CHANGE_USERNAME, (args, request) => changeUsername(socket, args, request));
  socket.on(GET_UPDATE_USER, (user, request) => getUpdateUser(socket, user, request));

  socket.on(LOGOUT, () => logout(socket))

});

getUpdateUser = (socket, user, request) => {
  const data = JSON.parse(user)

  users.forEach(user => {
    if (user.id === data.id && user.username !== data.username) {
      request(user)
    }
  })
}

changeUsername = (socket, args, request) => {
  let isAlreadyTaken = false
  const data = JSON.parse(args)
  const oldUser = data[0]
  const newUsername = data[1]
  console.log(`change username of ${oldUser.id} from ${oldUser.username} to ${newUsername}`);
  users.forEach(user => {
    if (newUsername === user.username) {
      console.log(newUsername + "is already exist");
      request(CHANGE_USERNAME_FAILED)
      io.emit(CHANGE_USERNAME_FAILED);

      isAlreadyTaken = true
    }
  })

  if (!isAlreadyTaken) {
    users.forEach(user => {
      if (user.username === oldUser.username) {
        user.username = newUsername
      }
    })

    console.log(newUsername);
    request(CHANGE_USERNAME_SUCCESS)
    // socket.emit(CHANGE_USERNAME_SUCCESS, newUsername)
    io.to(`${oldUser.id}`).emit(CHANGE_USERNAME_SUCCESS, newUsername);
  }
}

getReminders = (user, socket) => {
  console.log(users)
  console.log("user",user)
  const userData = JSON.parse(user)
  console.log("userData != null", userData != null)
  if (userData != null) {
    console.log(`send reminders of `);
    console.log(`user: ${userData.username}`);
    console.log(`user: ${userData.password}`);
    console.log(`user: ${userData.id}`);

    const filterReminders = reminders.filter((reminder) => reminder.username === userData.id)
    console.log(`send : ${filterReminders.length} reminders to ${userData.username}`);
    socket.emit(RECEIVE_REMINDERS, filterReminders)
  }
}

login = (user, request, socket) => {
  console.log(`try to login : ${user}`)

  const userData = JSON.parse(user)
  const {username, password} = userData

  if (isUserExist(username)) {
    if (!isCorrectPassword(userData)) {
      console.log(`password : ${password} is incorrect`)
      request(LOGIN_FAILED)
      return
    } else {
      console.log("!!!!!!!!!!!!!!!!!!!!!!", userData)
      if (userData.id === "null" || userData.id === undefined) {
        userData.id = userId(username)
      }
      socket.join(userData.id)
    }
  } else {
    userData.id = uuid()
    users.push(userData)
    socket.join(userData.id)

    console.log(`create user: ${user}`)
  }
  console.log(request)
  request(LOGIN_SUCCESS, userData)
  console.log(`success to login: ${userData}`)

  console.log(socket.id)
  socket.userid = userData.id
  console.log("socket.userid", socket.userid)
  console.log(`Username : ${username} joined Room Name : ${username}`)
}

logout = socket => {
  // if (socket !== undefined) {
  console.log(`try to logout`)
  console.log(socket.id)
  const id = socket.userid
  socket.leave(socket.userid)
  //     socket.emit(LOGOUT_SUCCESS, LOGOUT_SUCCESS)
  console.log(`Username : ${id} leave Room  : ${id}`)
  // }
}

deleteReminder = (deleteReminder, socket) => {
  console.log("deleteReminder",deleteReminder)
  const reminderData = JSON.parse(deleteReminder)
  console.log("reminderData",reminderData)

  console.log(`try to delete reminder: ${reminderData.name}`)

  reminders = reminders.filter((reminder) => reminder.id !== reminderData.id)
  io.to(`${reminderData.username}`).emit(DELETED_REMINDER, reminderData);
}

addReminder = (reminder) => {
  const reminderData = JSON.parse(reminder)
  console.log(`try to add reminder: ${reminderData.id}`)
  console.log(`try to add reminder of: ${reminderData.username}`)

  reminders.push(reminderData)
  io.to(`${reminderData.username}`).emit(ADDED_REMINDER, newReminder(reminderData));
}


newReminder = reminder => {
  console.log(`send new reminder ${reminder.id}`);

  return JSON.stringify(reminder)
}


updateReminder = (reminder) => {
  const reminderData = JSON.parse(reminder)

  console.log(`update reminder ${reminderData.id}`);
  findReminderAndReplace(reminderData)

  console.log(`send replace reminder ${reminderData.id}`);
  io.to(`${reminderData.username}`).emit(UPDATED_REMINDER, JSON.stringify(reminderData));

  return JSON.stringify(reminderData)
}

http.listen(port, () => {
  console.log(`Socket.IO server running at http://localhost:${port}/`);
});

findReminderAndReplace = (updateReminder) => {
  var i;

  for (i = 0; i < reminders.length; i++) {
    if (reminders[i].id === updateReminder.id) {
      reminders[i] = updateReminder
    }
  }
}

isCorrectPassword = ({username, password}) => {
  var i;
  for (i = 0; i < users.length; i++) {
    if (users[i].username === username && users[i].password === password) {
      return true;
    }
  }

  return false;
}

isUserExist = (username) => {
  var i;
  for (i = 0; i < users.length; i++) {
    if (users[i].username === username) {
      return true;
    }
  }

  return false;
}

userId = (username) => {
  console.log("@@@@@@@@@@@")
  var i;
  for (i = 0; i < users.length; i++) {
    if (users[i].username === username) {
      console.log("users[i].username ", users[i].id )
      return users[i].id;
    }
  }
}